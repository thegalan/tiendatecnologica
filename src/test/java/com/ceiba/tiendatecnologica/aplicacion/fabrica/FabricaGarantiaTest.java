package com.ceiba.tiendatecnologica.aplicacion.fabrica;

import com.ceiba.tiendatecnologica.aplicacion.comando.ComandoProducto;
import com.ceiba.tiendatecnologica.dominio.GarantiaExtendida;
import junit.framework.TestCase;

import java.util.Date;

public class FabricaGarantiaTest extends TestCase {

    public void testPrecioGarantiaIgual20DelPrecioProducto() {
        //Arrange
        ComandoProducto comandoProducto = new ComandoProducto("CODE","Apple Mac",600000);
        FabricaGarantia fabricaGarantia = new FabricaGarantia();

        // Act
        GarantiaExtendida garantiaExtendida = fabricaGarantia.crearGarantia(comandoProducto,"Raul");

        // Assert
        assertEquals(120000d,garantiaExtendida.getPrecioGarantia());
    }

    public void testPrecioGarantiaIgual10DelPrecioProducto() {
        //Arrange
        ComandoProducto comandoProducto = new ComandoProducto("CODE","Apple Mac",500000);
        FabricaGarantia fabricaGarantia = new FabricaGarantia();

        // Act
        GarantiaExtendida garantiaExtendida = fabricaGarantia.crearGarantia(comandoProducto,"Raul");

        // Assert
        assertEquals(50000d,garantiaExtendida.getPrecioGarantia());
    }
}