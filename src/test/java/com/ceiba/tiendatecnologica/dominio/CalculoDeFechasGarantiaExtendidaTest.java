package com.ceiba.tiendatecnologica.dominio;

import junit.framework.TestCase;

import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.mock;

public class CalculoDeFechasGarantiaExtendidaTest extends TestCase {

    public long obtenerDias(Date fechaFinGarantiaExtendida) {
        Date fechaInicio = new Date();

        Calendar fechaFinGarantia = Calendar.getInstance();
        Calendar fechaInicioGarantia = Calendar.getInstance();

        fechaFinGarantia.setTime(fechaFinGarantiaExtendida);
        fechaInicioGarantia.setTime(fechaInicio);

        /*****|Fecha Fin Garantia|*****/
        fechaFinGarantia.set(Calendar.MILLISECOND, 0);
        fechaFinGarantia.set(Calendar.MINUTE, 0);
        fechaFinGarantia.set(Calendar.HOUR, 0);
        fechaFinGarantiaExtendida = fechaFinGarantia.getTime();

        /*****|Fecha Inicio Garantia|*****/

        fechaInicioGarantia.set(Calendar.MILLISECOND, 0);
        fechaInicioGarantia.set(Calendar.MINUTE, 0);
        fechaInicioGarantia.set(Calendar.HOUR, 0);
        fechaInicio = fechaInicioGarantia.getTime();

        long diff = (fechaFinGarantiaExtendida.getTime() - fechaInicio.getTime())/(1000*60*60*24);
        return diff;
    }


    public void testCalcularFechaFinGrtiaMayorA200Dias() {
        /*****| Arrange |*****/
        CalculoDeFechasGarantiaExtendida calculoDeFechasGarantiaExtendida = new CalculoDeFechasGarantiaExtendida();

        /*****| Act |*****/
        Date date = calculoDeFechasGarantiaExtendida.calcularFechaFinGrtia(600000d);
        long diferenciaDias = this.obtenerDias(date);

        /*****| Assert |*****/
        assertTrue(diferenciaDias>200);
    }

    public void testCalcularFechaFinGrtiaIgualA100Dias() {
        /*****| Arrange |*****/
        CalculoDeFechasGarantiaExtendida calculoDeFechasGarantiaExtendida = new CalculoDeFechasGarantiaExtendida();

        /*****| Act |*****/
        Date date = calculoDeFechasGarantiaExtendida.calcularFechaFinGrtia(500000d);
        long diferenciaDias = this.obtenerDias(date);

        /*****| Assert |*****/
        assertTrue(diferenciaDias == 100);
    }

}