package com.ceiba.tiendatecnologica.dominio;

import java.util.Calendar;
import java.util.Date;

public class CalculoDeFechasGarantiaExtendida {

    public CalculoDeFechasGarantiaExtendida() {
    }

    public Date calcularFechaFinGrtia(double precioProducto){
        Date dateInstance = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateInstance);

        if (precioProducto > 500000) {
            for (int i = 0; i < 200; i++) {
                if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                } else calendar.add(Calendar.DAY_OF_YEAR, 2);
            }

            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                calendar.add(Calendar.DAY_OF_YEAR, 2);
            }
        }
        else {
            calendar.add(Calendar.DAY_OF_YEAR,100);
        }
        dateInstance = calendar.getTime();
        return dateInstance;
    }

    public Date calcularFechaInicioGrtia()  {
        Date fechaInicio = new Date();
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fechaInicio);
        fechaInicio = calendar.getTime();

        return fechaInicio;
    }
}
