package com.ceiba.tiendatecnologica.dominio;

public class CalculoPrecioGarantiaExtendida {

    public CalculoPrecioGarantiaExtendida() {
    }

    public Double calcularPrecioGarantia(Double precioProducto){
        double precioGarantia;
        if(precioProducto > 500000){
            precioGarantia = precioProducto*0.2;
        }
        else precioGarantia = precioProducto*0.1;
        return precioGarantia;
    }
}
